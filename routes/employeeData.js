var express = require('express');
var router = express.Router();

var convertStringToEmployeeData = function(data) {
  return {
    employeeId: data.employeeId,
    departMentId: data.departMentId,
    text: data.text,
    date: data.date
  };
}

//replace this with database

var employeeData = [
  {date: new Date("06-20-2017"), employeeId: 1, departMentId: 1, text: "this is some data"},
  {name: new Date("06-20-2017"), employeeId: 2, departMentId: 1, text: "asdasdasd asdadasd"},
  {name: new Date("06-20-2017"), employeeId: 3, departMentId: 2, text: "asdasdasdasd adsasdasd"},
  {name: new Date("06-20-2017"), employeeId: 4, departMentId: 1, text: "adsasdasdasd asdasdasda"},
  {name: new Date("06-20-2017"), employeeId: 5, departMentId: 2, text: "asdasdasdasd asdasdasdasd"}
];

//

router.post('/add', function(req, res, next) {
  employeeData.push(convertStringToEmployeeData(req.body));
  res.send("employee data added");
});

router.post("/delete", function(req, res, next) {
  res.send("employee data deleted");  
});

router.post("/get", function(req, res, next) {
  res.send(employeeData);  
});

module.exports = router;
