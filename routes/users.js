var express = require('express');
var router = express.Router();

var convertStringToUser = function(user) {
  return {
    name: user.name,
    employeeId: user.employeeId,
    departMentId: user.departMentId,
    role: user.role,
    password: user.password
  };
}

//replace this with database

var users = [
	{name: "Non Admin 1", employeeId: 1, departMentId: 1, role: 'U', password: 12345},
	{name: "Non Admin 2", employeeId: 2, departMentId: 1, role: 'U', password: 12345},
	{name: "Non Admin 3", employeeId: 3, departMentId: 2, role: 'U', password: 12345},
	{name: "Admin 1", employeeId: 4, departMentId: 1, role: 'A', password: 12345},
	{name: "Admin 2", employeeId: 5, departMentId: 2, role: 'A', password: 12345}
];

//

router.post('/add', function(req, res, next) {
	users.push(convertStringToTask(req.body));
  res.send("user added");
});

router.post("/delete", function(req, res, next) {
  res.send("user deleted");  
});


router.post('/validate', function(req, res, next) {
	var loginUser = convertStringToUser(req.body);
	var response = {
		status: "Failure",
		reason: "Login Failed"
	};
	for(var i =0; i < users.length; i++){
		var user = users[i];
		if(user.employeeId == loginUser.employeeId && user.departMentId == loginUser.departMentId && user.password == loginUser.password ){
			response.status = "Success";
			response.user = user;
		}
	}
	console.log(response);
	res.send(response);
});

module.exports = router;
