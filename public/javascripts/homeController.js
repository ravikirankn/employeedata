var homeController = function($http, $filter, $document, $cookies) {
	var controller = this;
  	controller.user = {};

  	controller.user.name = $cookies.get("name");
  	controller.user.employeeId = $cookies.get("employeeId");
  	controller.user.departMentId = $cookies.get("departMentId");
  	controller.user.password = $cookies.get("password");

  	console.log(controller.user);
  	var onSuccess = function(response){
  		if(response === undefined || response.data === undefined || response.data.status !== "Success"){
  			window.location.href = "logout.html";
  		}else{
  			controller.user.role = response.data.user.role;
  		}
  	}

  	var onFailure = function(error){
  		window.location.href = "logout.html"
  	};

  	$http.post('users/validate', controller.user)
         .then(onSuccess, onFailure);
}

angular.module('employeeProject', ['ngCookies'])
       .controller('HomeController', homeController);