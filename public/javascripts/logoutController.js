var logoutController = function($http, $filter, $document, $cookies) {
	var controller = this;
  	$cookies.remove("name");
    $cookies.remove("employeeId");
    $cookies.remove("departMentId");
    $cookies.remove("password");
};

angular.module('employeeProject', ['ngCookies'])
       .controller('LogoutController', logoutController);