var loginController = function($http, $filter, $document, $cookies) {
  var controller = this;
  controller.errorMessage = "";

  var onSuccess = function(response) {
  	console.log(response);
    if(response !== undefined && response.data !== undefined && response.data.status === "Success"){
    	var expireDate = new Date(new Date().getTime() + 15*60000);
    	$cookies.put("name", response.data.user.name, {'expires': expireDate} );
    	$cookies.put("employeeId", response.data.user.employeeId , {'expires': expireDate} );
    	$cookies.put("departMentId", response.data.user.departMentId , {'expires': expireDate} );
    	$cookies.put("password", response.data.user.password, {'expires': expireDate}  );
        window.location.href = "home.html";
    }else{
      controller.errorMessage = " Login Failure ";
    }
  };

  var onFailure = function(error){
    controller.errorMessage = " Login Failure ";
  }
  
  var login = function(user) {
    $http.post('users/validate', user)
         .then(onSuccess, onFailure);
  };

  controller.login = login;
}

angular.module('employeeProject', ['ngCookies'])
       .controller('LoginController', loginController);